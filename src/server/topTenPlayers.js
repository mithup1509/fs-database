function topTenPlayers(){
    return `SELECT p.playername,count(p.event like('%G%'))/count (p.playername) as average 
    FROM WORLDCUPPLAYERS as p 
    group by p.playername
    having count(p.playername)>5
    order by average desc
    limit 10  
    `;
}
module.exports=topTenPlayers;
// SELECT  P1.PLAYERNAME,COUNT(P1.EVENT LIKE('%G%'))/COUNT(P2.PLAYERNAME) AS AVERAGE 
// FROM WORLDCUPPLAYERS P1 JOIN WORLDCUPPLAYERS P2 ON P1.MATCHID=P2.MATCHID
// GROUP BY P2.PLAYERNAME,p1.playername
// ORDER BY AVERAGE DESC
// LIMIT 10

// SELECT p.playername,count(p.event)/count (p.playername) as average FROM WORLDCUPPLAYERS as p 
// where p.playername in(
// SELECT p.playername FROM WORLDCUPPLAYERS as p  where p.event like('%G%') group by p.playername)
// group by p.playername
// having count(p.playername)>5
// order by average desc
// limit 10  