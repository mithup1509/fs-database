const { Client } = require("pg");
const numberOfMathesPerCity = require('./src/server/numberofMatchesPerCity.js');
const fs = require('fs')
const path = require('path');
const numberOfMathesPerWonCity=require('./src/server/numberofMatchesWonPerTeam');
const numberOfRedCardsPerTeam=require('./src/server/numberofRedCardsPerTeam');
const topTenPlayers=require('./src/server/topTenPlayers');
const client = new Client({
  host: "localhost",
  user: "postgres",
  port: 5432,
  password: "1509",
  database: "postgres",
});

client.connect();


client.query(numberOfMathesPerCity(), (err, res) => {
  if (err) {
    console.error(err);
  } else {
    
    fs.writeFileSync(path.join(__dirname, 'src/output/numberOfMathesPerCity.json'), JSON.stringify(res.rows), "utf-8")
  }
});

client.query(numberOfMathesPerWonCity(), (err, res) => {
    if (err) {
      console.error(err);
    } else {
      
      fs.writeFileSync(path.join(__dirname, 'src/output/numberOfMathesPerWonCity.json'), JSON.stringify(res.rows), "utf-8")
    }
  }
);


client.query(numberOfRedCardsPerTeam(), (err, res) => {
  if (err) {
    console.error(err);
  } else {
    
    fs.writeFileSync(path.join(__dirname, 'src/output/numberOfRedCardsPerTeam.json'), JSON.stringify(res.rows), "utf-8")
  }
});

client.query(topTenPlayers(), (err, res) => {
  if (err) {
    console.error(err);
  } else {
    fs.writeFileSync(path.join(__dirname, 'src/output/topTenPlayers.json'), JSON.stringify(res.rows), "utf-8")
  }
});


